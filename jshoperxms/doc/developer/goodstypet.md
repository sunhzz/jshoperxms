商品管理－－商品类型&参数管理
===
##### 作者：JC.CHEN 日期：2015-10-12 版本：1.0

#### 功能简介
- 商品模块中的子模块，用来定义商品类型和该类型包含的参数。

#### 数据模型
    源码路径:
    /jshoperxms/src/com/jshoperxms/entity/GoodsTypeT.java
    @Id
    @Column(name="GOODS_TYPE_ID")
    private String goodsTypeId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createtime;

    private String creatorid;

    //JSON形式的参数对象
    @Column(name="GOODS_PARAMETER")
    private String goodsParameter;

    //商品类型名称
    private String name;

    private String status;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updatetime;

    private int versiont;


#### 视图
    源码路径:
    /jshoperxms/WebContent/admin/app/goods/goodstypement.html
    /jshoperxms/WebContent/admin/app/goods/goodstype.html

#### 控制层
    源码路径:
    /com/jshoperxms/action/mall/backstage/goods/GoodsTypeTAction.java
    /jshoperxms/WebContent/admin/app/js/goodstype/goodstypectrl.js

    //列表查询
    goodstypemodule.controller('goodstypelist',ServerGoodsTypeListCtrl);

    //添加和编辑
    goodstypemodule.controller('goodstype',ServerSaveGoodsType);

    //所有业务控制均采用angularJs调用后台java方法，并统一返回JSON对象

#### 路由控制
    源码路径:
    /jshoperxms/WebContent/admin/app/js/routes.js
    /**
     *商品类型管理路由
     */
    $routeProvider.when('/goodstypement',{
    	templateUrl:'app/goods/goodstypement.html',
    	controller:'goodstypelist'
    });
    $routeProvider.when('/goodstype',{
    	templateUrl:'app/goods/goodstype.html',
    	controller:'goodstype'
    });

#### 插件使用
- angularjs-datatables.js
