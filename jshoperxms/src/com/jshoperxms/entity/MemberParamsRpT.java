package com.jshoperxms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the member_params_rp_t database table.
 * 
 */
@Entity
@Table(name="member_params_rp_t")
@NamedQuery(name="MemberParamsRpT.findAll", query="SELECT m FROM MemberParamsRpT m")
public class MemberParamsRpT implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createtime;

	private String gkey;

	private String gvalue;

	private String memberid;

	private int status;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updatetime;

	private int versiont;

	public MemberParamsRpT() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreatetime() {
		return this.createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public String getGkey() {
		return this.gkey;
	}

	public void setGkey(String gkey) {
		this.gkey = gkey;
	}

	public String getGvalue() {
		return this.gvalue;
	}

	public void setGvalue(String gvalue) {
		this.gvalue = gvalue;
	}

	public String getMemberid() {
		return this.memberid;
	}

	public void setMemberid(String memberid) {
		this.memberid = memberid;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getUpdatetime() {
		return this.updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	public int getVersiont() {
		return this.versiont;
	}

	public void setVersiont(int versiont) {
		this.versiont = versiont;
	}

}