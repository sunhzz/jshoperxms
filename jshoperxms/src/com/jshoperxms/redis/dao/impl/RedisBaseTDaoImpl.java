package com.jshoperxms.redis.dao.impl;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.jshoperxms.redis.dao.RedisBaseTDao;

@Repository("redisBaseTDao")
public class RedisBaseTDaoImpl<T> implements RedisBaseTDao<T> {
	@Autowired
	private RedisTemplate<String, T> redisTemplate;

	@Override
	public void put(String key, final T t, Class<T> cls) {
		redisTemplate.opsForHash().put(key, key, t);
	}

	@Override
	public T get(String key, Class<T> cls) {
		return (T) redisTemplate.opsForHash().get(key, key);
	}

	@Override
	public void put(String key, T t, Class<T> cls, long timeout,TimeUnit timeUnit) {
		redisTemplate.opsForHash().put(key, key, t);
		redisTemplate.expire(key, timeout, timeUnit);
	}

	@Override
	public void removed(String key,Class<T> cls) {
		redisTemplate.opsForHash().delete(key, key);
	}


}
