package com.jshoperxms.service.impl;


import javax.annotation.Resource;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jshoperxms.action.utils.BaseTools;
import com.jshoperxms.action.utils.enums.BaseEnums.DataUsingState;
import com.jshoperxms.dao.BrandTDao;
import com.jshoperxms.entity.BrandT;
import com.jshoperxms.entity.GoodsTypeBrandT;
import com.jshoperxms.service.BrandTService;
import com.jshoperxms.service.GoodsTypeBrandTService;

@Service("brandTService")
@Scope("prototype")
public class BrandTServiceImpl extends BaseTServiceImpl<BrandT>implements BrandTService {
	@Resource
	private BrandTDao brandTDao;
	@Resource
	private GoodsTypeBrandTService goodsTypeBrandTService;
	@Resource
	private Serial serial;
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public boolean saveBrand(BrandT brand,String goodsTypeId,String goodsTypeName) {
		//1,检测品牌名称是否存在
		boolean sucflag = false;
		Criterion criterion=Restrictions.eq("brandname", brand.getBrandname());
		BrandT bt=this.brandTDao.findOneByCriteria(BrandT.class, criterion);
		if(bt==null){
			//2,保存品牌
			this.brandTDao.save(brand);
			//3,保存类型品牌
			GoodsTypeBrandT gtb=new GoodsTypeBrandT();
			gtb.setGoodsTypeBrandTid(this.serial.Serialid(Serial.GOODSTYPEBRAND));
			gtb.setBrandid(bt.getId());
			gtb.setBrandname(bt.getBrandname());
			gtb.setCreatetime(BaseTools.getSystemTime());
			gtb.setGoodsTypeId(goodsTypeId);
			gtb.setGoodsTypeName(goodsTypeName);
			gtb.setStatus(DataUsingState.USING.getState());
			gtb.setUpdatetime(BaseTools.getSystemTime());
			gtb.setVersiont(1);
			this.goodsTypeBrandTService.save(gtb);
			sucflag=true;
		}
		return sucflag;
	}


}
