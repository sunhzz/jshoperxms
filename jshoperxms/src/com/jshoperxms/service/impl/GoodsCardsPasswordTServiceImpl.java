package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.GoodsCardsPasswordT;
import com.jshoperxms.service.GoodsCardsPasswordTService;

@Service("goodsCardsPasswordTService")
@Scope("prototype")
public class GoodsCardsPasswordTServiceImpl extends BaseTServiceImpl<GoodsCardsPasswordT> implements GoodsCardsPasswordTService {


}
