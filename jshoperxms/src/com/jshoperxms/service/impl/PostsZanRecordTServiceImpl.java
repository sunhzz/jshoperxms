package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.PostsZanRecordT;
import com.jshoperxms.service.PostsZanRecordTService;

@Service("postsZanRecordTService")
@Scope("prototype")
public class PostsZanRecordTServiceImpl extends BaseTServiceImpl<PostsZanRecordT> implements PostsZanRecordTService {

}
