package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.ShippingAddressT;
import com.jshoperxms.service.ShippingAddressTService;

@Service("shippingAddressTService")
@Scope("prototype")
public class ShippingAddressTServiceImpl extends BaseTServiceImpl<ShippingAddressT>implements ShippingAddressTService {

}
