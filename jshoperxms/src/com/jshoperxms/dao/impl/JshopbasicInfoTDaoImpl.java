package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.JshopbasicInfoTDao;
import com.jshoperxms.entity.JshopbasicInfoT;

@Repository("jshopbasicInfoTDao")
public class JshopbasicInfoTDaoImpl extends BaseTDaoImpl<JshopbasicInfoT> implements JshopbasicInfoTDao {
	

	private static final Logger log = LoggerFactory.getLogger(JshopbasicInfoTDaoImpl.class);



}