package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.GoodsTypeBrandTDao;
import com.jshoperxms.entity.GoodsTypeBrandT;


@Repository("goodsTypeBrandTDao")
public class GoodsTypeBrandTDaoImpl extends BaseTDaoImpl<GoodsTypeBrandT> implements GoodsTypeBrandTDao {
	
	private static final Logger log = LoggerFactory.getLogger(GoodsTypeBrandTDaoImpl.class);
	

}