package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.GoodsTypeTDao;
import com.jshoperxms.entity.GoodsTypeT;


@Repository("goodsTypeTDao")
public class GoodsTypeTDaoImpl extends BaseTDaoImpl<GoodsTypeT> implements GoodsTypeTDao {
	
	private static final Logger log = LoggerFactory.getLogger(GoodsTypeTDaoImpl.class);

	
}