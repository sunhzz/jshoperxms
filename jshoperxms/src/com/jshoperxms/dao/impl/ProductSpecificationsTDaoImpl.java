package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.ProductSpecificationsTDao;
import com.jshoperxms.entity.ProductSpecificationsT;


@Repository("productSpecificationsTDao")
public class ProductSpecificationsTDaoImpl extends BaseTDaoImpl<ProductSpecificationsT> implements ProductSpecificationsTDao{
	
	private static final Logger log = LoggerFactory.getLogger(ProductSpecificationsTDaoImpl.class);

}