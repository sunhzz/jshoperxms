package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.OrderTDao;
import com.jshoperxms.entity.OrderT;


@Repository("orderTDao")
public class OrderTDaoImpl extends BaseTDaoImpl<OrderT> implements OrderTDao {

	private static final Logger log = LoggerFactory.getLogger(OrderTDaoImpl.class);


}