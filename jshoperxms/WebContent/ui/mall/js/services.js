define([ 'angular' ], function(angular) {
	'use strict';
	var myservices = angular.module('myservices',[]);
	//检查用户本地session保存的服务
	myservices.service('sessionService', ['$window',function($window) {
		//从本地获取保存的x-session－token
		this.gologin=false;
		if($window.localStorage){
			this.xsessiontoken=localStorage.getItem("x-session-token");
			if(this.xsessiontoken==null||this.xsessiontoken==undefined){
				this.gologin=true;
			}
		}
	}]);
	return myservices;
	
});
