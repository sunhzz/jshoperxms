$(function(){
	//新增收货地址	
	addnewaddress = function(basepath){
		//省
		var province = $("#provincenew").val();
		//市
		var city = $("#citynew").val();
		//县
		var area = $("#areanew").val();
		//街道
		var street= $("#streetnew").val();
		//收货人姓名
		var shippingusername = $("#shippingusername").val();
		//收货人电话
		var mobile = $("#mobile").val();
		//收货人邮编
		var postcode = $("#postcode").val();
		//别名
		var addresstag = $("#addresstag").val();
		$.post(basepath+"/mcenter/savedeliveraddress.action",
			{
	                              "province" : province,
	                              "city" : city,
	                              "district" : area,
	                              "street" : street,
	                              "shippingusername" : shippingusername,
	                              "mobile" : mobile,
	                              "postcode" : postcode,
	                              "country" : "中国",
	                              "addresstag":addresstag
	                            },function(data) {
	                            	if (data.slogin) {
					if (data.sucflag) {
						alert("添加成功！");
	                            			location.reload();
	                            		}
	                            	}else{
	                            		window.location.href = basepath+"/gologin.action";
	                            	}
	                            	
	               });		
	};
	//根据Id删除收货地址
	deladdressById = function(basepath,id){
		$.post(basepath+"/mcenter/deladdressById.action",
			{"id":id},
			function(data){
			if (data.slogin) {
				if (data.sucflag) {
					alert("删除成功！");
					location.reload();
				}
			}else{
				window.location.href = basepath+"/gologin.action";
			}
		});
	};
	//根据id修改收货地址
	updateAddressById = function(basepath,index,id){
		//省
		var province = $("#province"+index).val();
		//市
		var city = $("#city"+index).val();
		//县
		var area = $("#area"+index).val();
		//街道
		var street= $("#street"+index).val();
		//收货人姓名
		var shippingusername = $("#shippingusername"+index).val();
		//收货人电话
		var mobile = $("#mobile"+index).val();
		//收货人邮编
		var postcode = $("#postcode"+index).val();
		//别名
		var addresstag = $("#addresstag"+index).val();
		$.post(basepath+"/mcenter/updateAddressById.action",
			{
			    "id":id,
	                              "province" : province,
	                              "city" : city,
	                              "district" : area,
	                              "street" : street,
	                              "shippingusername" : shippingusername,
	                              "mobile" : mobile,
	                              "postcode" : postcode,
	                              "country" : "中国",
	                              "addresstag":addresstag
	                            },function(data) {
	                            	if (data.slogin) {
					if (data.sucflag) {
						alert("修改成功！");
	                            			location.reload();
	                            		}
	                            	}else{
	                            		window.location.href = basepath+"/gologin.action";
	                            	}
	                            	
	               });

	};

})